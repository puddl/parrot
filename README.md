# Installation
We're still in dev. Please be kind. :)
```
cd $PUDDL_HOME
git clone https://gitlab.com/puddl/parrot.git
cd parrot
pip install -e .

puddl app add puddl_parrot
puddl migrate
```

Try it!
```
echo '{"hello": "world"}' | puddl parrot -
```
