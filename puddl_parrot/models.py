from django.contrib.postgres.fields import JSONField

from puddl.models import Datum


class Parrot(Datum):
    data = JSONField()
