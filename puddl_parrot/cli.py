import json

import click

from puddl_parrot import models


@click.command(name='parrot')
@click.argument('JSON_FILE', type=click.File('r'))
def main(json_file):
    """Load some JSON data. Use "-" to consume STDIN.

    Examples:

        puddl ingress parrot puddl_parrot/tests/data/foobar.json
        cat puddl_parrot/tests/data/foobar.json | puddl ingress parrot -
    """
    data = json.load(json_file)
    models.Parrot.objects.create(data=data)
