from django.apps import AppConfig


class PuddlParrotConfig(AppConfig):
    name = 'puddl_parrot'
    label = 'parrot'
